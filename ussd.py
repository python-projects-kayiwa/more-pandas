import pandas as pd 
import sys

original_stdout = sys.stdout

url = './sept_ussd_responses.csv'

dataframe = pd.read_csv(url)

dataframe1 = dataframe.drop(['Session_id','Phone_Number','Department','Rating','Rating_Reason','created_at','ussd_code','LastVisit','Waiting_time'], axis=1)

# x = dataframe1['Department'].value_counts()

x = dataframe1.describe()
y = dataframe1['Language'].value_counts()
z = dataframe1['Station_Category'].value_counts()
a = dataframe1['status'].value_counts()

x.to_excel('septussddata.xlsx')
with open('septussddata.txt','w') as f:
    sys.stdout = f
    print("The data on the ussd responses for the month of October 2020")
    print(x)
    print('')
    print(y)
    print('')
    print(z)
    print('')
    print(a)
    sys.stdout = original_stdout